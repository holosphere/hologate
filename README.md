# holoGate


holoGates are entry points to the [holoSphere][1] planetary OS

framaPad: <https://annuel2.framapad.org/p/holoGate>


## holoservers

 - [holoservers](holoservers.md)

## holoGateWay

 - <https://ipfs.blockring™.ml>


[1]: https://qwant.com/?q=holoSphere+planetary+OS+%26
